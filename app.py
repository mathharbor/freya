import os
import os.path as op
import subprocess
import hashlib

from flask import Flask, url_for, redirect, render_template, request, session, flash

from flask_peewee.auth import Auth
from flask_peewee.auth import BaseUser
from flask_peewee.db import Database
from peewee import *
from flask_peewee.admin import Admin
from flask_peewee.admin import ModelAdmin

from flask_debugtoolbar import DebugToolbarExtension


# configure our database
DATABASE = {
    'name': 'test.db',
    'engine': 'peewee.SqliteDatabase',
}
DEBUG = True
SECRET_KEY = 'ssshhhh'
app = Flask(__name__)
app.config.from_object(__name__)
app.config['DEBUG_TB_INTERCEPT_REDIRECTS'] = False
app.config['DEBUG_TB_PROFILER_ENABLED'] = True

# instantiate the db wrapper
db = Database(app)

toolbar = DebugToolbarExtension(app)


# create our custom user model note that we're mixing in the BaseModel in order to
# use the default auth methods it implements, "set_password" and "check_password"
class User(db.Model, BaseUser):
    username = CharField()
    password = CharField()
    email = CharField()
    name = CharField()
    active = BooleanField(default=True)
    is_superuser = BooleanField(default=False)

    def __unicode__(self):
        return self.username

# create a modeladmin for it
class UserAdmin(ModelAdmin):
    columns = ('username', 'email', 'password', 'name', 'is_superuser',)


# subclass Auth so we can return our custom classes
class CustomAuth(Auth):
    def get_user_model(self):
        return User

    def get_model_admin(self):
        return UserAdmin

class Container(db.Model):
    docker_id = CharField()
    name = CharField()
    image = CharField()
    memory = CharField()
    graphterm_port = CharField()
    ipython_port = CharField()
    sage_port = CharField()
    owner = ForeignKeyField(User)

    def __unicode__(self):
        return self.docker_id

class ContainerAdmin(ModelAdmin):
	columns = ('docker_id', 'name', 'image', 'memory', 'graphterm_port', 'ipython_port', 'sage_port', 'owner',)

class Collaboration(db.Model):
	container_id = ForeignKeyField(Container)
	owner = ForeignKeyField(User, related_name='owned_containers')
	collaborator = ForeignKeyField(User, related_name='shared_containers')

	def __unicode__(self):
		return '%s shared %s with %s' % (self.owner, self.docker_id, self.collaborator)

class CollaborationAdmin(ModelAdmin):
	columns = ('container_id', 'owner', 'collaborator',)

class Snapshot(db.Model):
	docker_id = ForeignKeyField(Container)
	image_id = CharField()
	owner = ForeignKeyField(User)

	def __unicode__(self):
		return self.image_id

class SnapshotAdmin(ModelAdmin):
	columns = ('docker_id', 'image_id', 'owner',)

class CustomAdmin(Admin):
    def check_user_permission(self, user):
        return user.is_superuser



#Initialize auth and admin modules.

auth = CustomAuth(app, db)
admin = CustomAdmin(app, auth)
admin.register(User, UserAdmin)
admin.register(Container, ContainerAdmin)
admin.register(Collaboration, CollaborationAdmin)
admin.register(Snapshot, SnapshotAdmin)
admin.setup()

# App routes. 

@app.route('/')
def index_view():
	return redirect('/dashboard')

@app.route('/register', methods=['GET', 'POST'])
def register_view():
	if request.method == 'POST' and request.form['inputUsername3']:
		try:
            # use the .get() method to quickly see if a user with that name exists
			user = User.get(username=request.form['inputUsername3'])
        	#flash('That username is already taken.')
			return redirect('/register')
		except User.DoesNotExist:
            # if not, create the user and store the form data on the new model
			user = User(username=request.form['inputUsername3'], email=request.form['inputEmail3'], name=request.form['inputName3'])
			user.set_password(request.form['inputPassword3'])
			user.save()
			auth.login_user(user)
			return redirect('/dashboard')
	return render_template('register.html')


@app.route('/dashboard')
@auth.login_required
def dashboard_view():
	user = auth.get_logged_in_user()
	#containers = Container.get(owner=user)
	return render_template('mycontainers.html')#, containers=containers)


@app.route('/create_container', methods=['GET', 'POST'])
@auth.login_required
def create_container_view():
	user = auth.get_logged_in_user()
	if request.method == 'POST':
		container_name = request.form['inputName3']
		container_memory = request.form['inputMemory3']
		c = subprocess.check_output("sudo docker run -d -i -t ubuntu /bin/bash", shell=True)
		docker_id = c.strip('\n')
		container = Container(docker_id=docker_id, name=container_name, memory=container_memory, image='ubuntu', graphterm_port=2000, sage_port=3000, ipython_port=5000, owner=user)
		container.save()
		return redirect('/dashboard')
	return render_template('createcontainer.html')







if __name__ == '__main__':
	app.run(host='0.0.0.0', port=80, debug=True)
