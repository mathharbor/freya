'''
Small test app for testing:
- storing models in Redis
- testing custom Docker functionality
- testing graphterm health.
'''

from app import *

def test_user_model():
	username = raw_input("Enter sample username:")
	name = raw_input("Enter sample user's name:")
	email = raw_input("Enter sample user email:")
	password = raw_input("Enter sample user password:")
	uid = hashlib.sha1(username).hexdigest()
	password = hashlib.sha512(password).hexdigest()
	t = User(user_id=uid, username=username, name=name, email=email, password=password)
	t.save()
	print t.user_id
	print t.username
	print t.name
	print t.email
	print t.password
	print "User saved in Redis successfully. Deleting keys now."
	t.delete()


if __name__ == '__main__':
	test_user_model()
